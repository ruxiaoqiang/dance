package com.example.demo;

import com.example.demo.rxq.Rxq;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
class DemoApplicationTests {
    Rxq rxq = new Rxq();
    @Test
    public void sayHello() {
        rxq.test();
    }

}
